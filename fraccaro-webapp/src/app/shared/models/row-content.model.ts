// this model describe the "content-row" component elements
// there will be more than one type of "content-row" described as follow:

export class RowContent {
  type: number;
  background: string;
  titleImage: string;
  title: string;
  separator: string;
  text: string;
  link: string;
  image: string;
}