// this model describe the structure of a single
// element of the "carousel" component

export class Carousel {
  image: string;
}