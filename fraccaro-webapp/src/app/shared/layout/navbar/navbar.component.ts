import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  logoPath: string;

  constructor() { }

  ngOnInit() {
    this.getAPINavbar();
  }

  getAPINavbar() {
    this.logoPath = "http://via.placeholder.com/350x150";
  }

}
