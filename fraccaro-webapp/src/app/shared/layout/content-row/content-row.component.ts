import { Component, OnInit, Input } from '@angular/core';

import { RowContent } from '../../models/row-content.model';

@Component({
  selector: 'app-content-row',
  templateUrl: './content-row.component.html',
  styleUrls: ['./content-row.component.css']
})
export class ContentRowComponent implements OnInit {

  @Input() rowContent: RowContent = new RowContent;

  constructor() { }

  ngOnInit() {
  }
}
