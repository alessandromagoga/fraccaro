import { Component, OnInit } from '@angular/core';
import { Meta, MetaDefinition, Title } from "@angular/platform-browser";
import { HttpClient } from '@angular/common/http';

//import { UserResponse } from '../shared/interfaces/test.interface'
import { Carousel } from '../shared/models/carousel.model';
import { RowContent } from '../shared/models/row-content.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // meta object
  metaObject: MetaDefinition[] = [];

  // carousel content
  myCarousel: Carousel[] = [];

  // homepage rows (section 1)
  homePageRows1: RowContent[] = [];

  // homepage rows (section 2)
  homePageRows2: RowContent[] = [];

  public constructor(private meta: Meta, private title: Title, private http: HttpClient) {
  }

  ngOnInit(): void {
    /*
    this.http.get<UserResponse>('https://api.github.com/users/seeschweiler').subscribe(data => {
      this.title.setTitle(data.login);
    });
    */

    this.getHomePageMetaTitle();
    this.getCarousel();
    this.getHomePageRows1();
    this.getHomePageRows2();
  }

  // call API to get home page meta and title
  getHomePageMetaTitle(): void {
    this.title.setTitle('Fraccaro Spumadoro Home Page!');
    this.meta.addTags(this.metaObject);

    /*
    this.meta.addTags([
      {name: 'description', content: 'This is the HomePage description!'}
    ]);
    */
  }

  // call API to get carousel contents
  getCarousel(): void {
    this.myCarousel = [];
    this.myCarousel.push(
      { image: "https://gnammo-media.s3.amazonaws.com/cache/b4/83/b483fe804cd09c3ab381fd6faea9558d.jpg" },
      { image: "https://i0.wp.com/www.hotellesoleil.it/wp-content/uploads/venezia-hd.jpg?fit=1920%2C1080" },
      { image: "https://www.mmcarline.it/images/content/venezia.jpg" },
      { image: "http://www.parcocapraro.it/wp-content/uploads/2015/06/Venezia-veduta-dal-canal-grande.jpg" }
    )
  }

  // call API to get home page rows (first section)
  getHomePageRows1(): void {
    this.homePageRows1 = [];
    this.homePageRows1.push(
      { type: 1, background: null, titleImage: "http://via.placeholder.com/250x100", title: null, separator: "http://via.placeholder.com/20x20", text: "Fraccaro Spumadoro nasce nel 1932, quando Elena e Giovanni Fraccaro fondarono un panificio fra le mura di Castelfranco dando inizio ad una tradizione dolciaria destinata a diventare rinomata nel tempo.", link: "/", image: "http://via.placeholder.com/500x350" },
      { type: 3, background: "http://via.placeholder.com/1920x400/444444/000000", titleImage: null, title: "IL LIEVITO MADRE", separator: "http://via.placeholder.com/125x20", text: "<< Dal 1932 la famiglia Fraccaro continua con entusiasmo e convinzione questo percorso storico. Fraccaro Spumadoro: da sempre fedeli alle tradizioni. >>", link: "/", image: "http://via.placeholder.com/350x350" }
    );
  }

  // call API to get home page rows (second section)
  getHomePageRows2(): void {
    this.homePageRows2 = [];
    this.homePageRows2.push(
      { type: 2, background: null, titleImage: "http://via.placeholder.com/250x100", title: null, separator: "http://via.placeholder.com/20x20", text: "Fraccaro Spumadoro nasce nel 1932, quando Elena e Giovanni Fraccaro fondarono un panificio fra le mura di Castelfranco dando inizio ad una tradizione dolciaria destinata a diventare rinomata nel tempo.", link: "/", image: "http://via.placeholder.com/500x350" },
      { type: 1, background: null, titleImage: "http://via.placeholder.com/250x100", title: null, separator: "http://via.placeholder.com/20x20", text: "Fraccaro Spumadoro nasce nel 1932, quando Elena e Giovanni Fraccaro fondarono un panificio fra le mura di Castelfranco dando inizio ad una tradizione dolciaria destinata a diventare rinomata nel tempo.", link: "/", image: "http://via.placeholder.com/500x350" },
      { type: 2, background: null, titleImage: "http://via.placeholder.com/250x100", title: null, separator: "http://via.placeholder.com/20x20", text: "Fraccaro Spumadoro nasce nel 1932, quando Elena e Giovanni Fraccaro fondarono un panificio fra le mura di Castelfranco dando inizio ad una tradizione dolciaria destinata a diventare rinomata nel tempo.", link: "/", image: "http://via.placeholder.com/500x350" }
    );
  }
}
