import { Component, Input, OnInit } from '@angular/core';

import { Carousel } from '../../shared/models/carousel.model'

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  @Input() carouselContent: Carousel[] = [];
  
  constructor() { }

  ngOnInit() {
  }

}
