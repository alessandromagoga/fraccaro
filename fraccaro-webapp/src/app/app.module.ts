import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { NavbarComponent } from './shared/layout/navbar/navbar.component';
import { CarouselComponent } from './home/carousel/carousel.component';
import { ContentRowComponent } from './shared/layout/content-row/content-row.component';
import { RowSeparatorComponent } from './shared/layout/row-separator/row-separator.component';
import { ProductsPreviewComponent } from './shared/layout/products-preview/products-preview.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    FooterComponent,
    NavbarComponent,
    CarouselComponent,
    ContentRowComponent,
    RowSeparatorComponent,
    ProductsPreviewComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'fraccaro-webapp-pre'}),
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
